<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>PHP; Perulangan</title>
</head>
<style type="text/css">
body{
    background-color:#a9f5e1;
    text-align:center;
}
#kotak{
    background-color: #24c64f;
    width:30%;
    margin-left:35%;
}
.tombol{
    margin-top: 10px;
}
.jawaban{
    background-color:#ffff;
}
</style>

<body>
    <div id="kotak">
        <p>BINTANG</p>
    <form action="" method="POST">
  Masukan bintang<input type="text" name="bintang">
  <br>
        <input type="submit" class="tombol" name="submit" value="Submit">
        </form>
        <br>
<div class="jawaban">        
<?php
    if(isset($_POST['submit'])){
        $bin = $_POST['bintang'];
        for($a=1; $a<=$bin; $a++){
            for($b=$bin; $b>=$a; $b-=1){
                echo(' ');
            }
            for($c=1; $c<=($a*2)-1; $c++){
                echo '*';
            }
            echo "<br/>";
        }
    }
?>
</div>
</body>
</html>